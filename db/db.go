package db

import (
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

func NewMssql(connstr string) *gorm.DB {
	db, err := gorm.Open(sqlserver.Open(connstr), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	return db
}

func NewMysql(connstr string) *gorm.DB {
	db, err := gorm.Open(mysql.Open(connstr), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	return db
}
