package db

import (
	"github.com/go-redis/redis"
)

//RedisNil .
const RedisNil = redis.Nil

//RedisClient .
type RedisClient struct {
	*redis.Client
}

type Connoption struct {
	Addr     string
	Password string
	DB       int
}

type Failover struct {
	MasterName    string
	SentinelAddrs []string
	Password      string
	DB            int
}

//NewRedisDB create redis client
//文档:  https://github.com/go-redis/redis
func NewRedisDB(cp Connoption) *RedisClient {
	return NewRedisDBByName("redis", cp)
}

// NewRedisDBByName .
func NewRedisDBByName(name string, config Connoption) *RedisClient {
	client := redis.NewClient(&redis.Options{
		Addr:     config.Addr,
		Password: config.Password, // no password set
		DB:       config.DB,       // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}

	return &RedisClient{
		Client: client,
	}
}

//NewFailoverRedisDB create redis client
//哨兵模式
//文档:  https://github.com/go-redis/redis
func NewFailoverRedisDB(config Failover) *RedisClient {
	return NewFailoverRedisDBByName("redis", config)
}

// NewFailoverRedisDBByName .
func NewFailoverRedisDBByName(name string, config Failover) *RedisClient {
	client := redis.NewFailoverClient(&redis.FailoverOptions{
		// The Master Name
		MasterName: config.MasterName,
		// A seed list of host:port addresses of sentinel nodes.
		SentinelAddrs: config.SentinelAddrs,

		// Following options are copied from Options struct
		Password: config.Password, // no password set
		DB:       config.DB,       // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}

	return &RedisClient{
		Client: client,
	}
}
