module gitlab.com/adoniscss/utils

go 1.18

require (
	github.com/go-redis/redis v6.15.9+incompatible
	gorm.io/driver/mysql v1.5.1
	gorm.io/driver/sqlserver v1.5.1
	gorm.io/gorm v1.25.4
)

require (
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/microsoft/go-mssqldb v1.1.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.10 // indirect
	golang.org/x/crypto v0.9.0 // indirect
)
